const express = require("express");
const router = express.Router();
const pacienteController = require("../controllers/pacienteControllers.js");

module.exports = function () {
    // * agrega pacientes
    router.post("/pacientes", pacienteController.nuevoCliente);
    // * obtiene pacientes
    router.get('/pacientes', pacienteController.obtenerPacientes)
    // * obtiene un paciente en especifico
    router.get('/pacientes/:id', pacienteController.obtenerPaciente)
    // * actualizar paciente
    router.put('/pacientes/:id', pacienteController.actualizarPaciente)
    // * eliminar paciente
    router.delete('/pacientes/:id', pacienteController.eliminarPaciente)
    return router;
};
