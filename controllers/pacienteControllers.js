const Paciente = require("../models/Paciente.js");

// ? Cuando se crea un nuevo cliente
exports.nuevoCliente = async (req, res, next) => {
    // * crear un obj con datos del paciente
    const paciente = new Paciente(req.body);

    try {
        await paciente.save();
        res.json({ mensjae: "Cliente agregado" });
    } catch (error) {
        console.error(error);
        next(); // * si ocurre un error no se quede acá
    }
};

exports.obtenerPacientes = async (req, res, next) => {
    try {
        const pacientes = await Paciente.find({});
        res.json(pacientes);
    } catch (error) {
        console.error(error);
        next();
    }
};

exports.obtenerPaciente = async (req, res, next) => {
    try {
        const paciente = await Paciente.findById(req.params.id);
        res.json(paciente);
    } catch (error) {
        console.error(error);
        next();
    }
};

exports.actualizarPaciente = async (req, res, next) => {
    try {
        const paciente = await Paciente.findOneAndUpdate(
            {
                _id: req.params.id, // * se busca por este ID
            },
            req.body, // * se actualiza con esta información
            {
                new: true, // * nos regresa los datos actualizados
            }
        );
        res.json(paciente);
    } catch (error) {
        console.error(error);
        next();
    }
};

exports.eliminarPaciente = async (req, res, next) => {
    try {
        await Paciente.findOneAndDelete({ _id: req.params.id });
        res.json({ mensaje: "Paciente eliminado" });
    } catch (error) {
        console.error(error);
        next();
    }
};
