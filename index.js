const express = require("express");
const mongoose = require("mongoose");
const routes = require("./routes");
const cors = require("cors")

// ? crear servidor
const app = express();

// ? habilitar cors 
app.use(cors())

// ? hablitar body parser
app.use(express.json());

// ? conectar a mongodb
mongoose.Promise = global.Promise;
mongoose.connect(
    "mongodb+srv://m001-student:m001-mongodb-basics@sandbox.vbgsi.mongodb.net/veterinaria?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
    }
);

// ? habilitar routing
app.use("/", routes()); // * son middlewares

// ? puerto y arranque
app.listen(4000, () => {
    console.log("Servidor iniciado");
});
